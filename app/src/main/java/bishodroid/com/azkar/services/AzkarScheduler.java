package bishodroid.com.azkar.services;

import android.content.Context;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import bishodroid.com.azkar.utils.AzkarPlayer;

/**
 * Created by user on 12/26/2016.
 */

public class AzkarScheduler {

    private static AzkarScheduler singleton;
    private ScheduledExecutorService scheduler;
    private ScheduledFuture future;
    private Context context;
    private static AzkarPlayer player;

    /**
     * Private constructor to create {@link AzkarScheduler} object
     * @param context
     */
    private AzkarScheduler(Context context){
        this.context = context;
    }

    /**
     * Create a singleton instance of {@link AzkarScheduler} to make sure we don't create too many objects
     * @param context
     * @return
     */
    public static AzkarScheduler getInstance(Context context){
        if (singleton == null) {
            singleton = new AzkarScheduler(context);
            player = AzkarPlayer.getInstance(context);
        }
        return singleton;
    }

    public void start(int interval){
        scheduler = Executors.newScheduledThreadPool(1);
        future = scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
            player.playNext();
            }
        },0,interval, TimeUnit.MINUTES);
    }

    public void stop(){
        future.cancel(false);
    }
}
