package bishodroid.com.azkar.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import bishodroid.com.azkar.utils.AppSettings;

/**
 * Created by user on 12/26/2016.
 */

public class AzkarService extends Service {

    private static AzkarService service;
    private AzkarScheduler scheduler;
    private AppSettings settings;
    /**
     * To create a single instance of the {@link AzkarService}
     * @param context
     * @return
     */
    public static AzkarService getInstance(Context context){
        if (service == null) service = new AzkarService();
        return service;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        scheduler = AzkarScheduler.getInstance(this);
        settings = AppSettings.getInstance(getApplicationContext());
        scheduler.start(settings.getAzkarFreqInMins());
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        scheduler.stop();
        this.stopSelf();
    }
}
