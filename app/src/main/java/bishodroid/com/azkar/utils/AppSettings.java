package bishodroid.com.azkar.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

import bishodroid.com.azkar.pojos.WrittenZikr;
import bishodroid.com.azkar.pojos.Zikr;

/**
 * Created by hp on 26/11/2016.
 */

public class AppSettings {

    private static AppSettings singleton;
    private static Context context;
    private static Gson gson;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    /**
     * private constructor to instantiate the {@link AppSettings}
     *
     * @param context
     */
    private AppSettings(Context context) {
        prefs = context.getSharedPreferences(Constants.APP_SETTINGS_NAME, Context.MODE_PRIVATE);
        editor = prefs.edit();
        this.context = context;
        this.gson = new Gson();
    }

    /**
     * Static method to create a singleton instance of the {@link AppSettings}
     *
     * @return a new {@link AppSettings} object if none was created before
     */
    public static AppSettings getInstance(Context context) {
        if (singleton == null) singleton = new AppSettings(context);
        return singleton;
    }

    /**************************************
     * Sets and gets the isFirstRun value
     *************************************/
    public boolean isFirstRun() {
        return prefs.getBoolean(Constants.FIRST_RUN_FLAG, true);
    }

    public void setIsFirstRun(boolean value) {
        editor.putBoolean(Constants.FIRST_RUN_FLAG, value).commit();
    }


    /**************************************
     * Sets and gets the App Lang value
     *************************************/
    public String getAppLang() {
        return prefs.getString(Constants.APP_LANG, Constants.LANG_EN);
    }
    public void setAppLang(String lang){
        editor.putString(Constants.APP_LANG,lang).commit();
    }

    /***************************************************************
     * Sets the flag to tell if the azkar service is running or not
     **************************************************************/
    public boolean isAzkarServiceRunning(){return prefs.getBoolean(Constants.AZKAR_SERVICE_FLAG,false);}
    public void setIsAzkarServiceRunning(boolean isAzkarServiceRunning){
        editor.putBoolean(Constants.AZKAR_SERVICE_FLAG,isAzkarServiceRunning).commit();
    }

    /**
     *Sets the frequency of azkar in minutes
     * This value is used by the Azkar service
     */
    public void setAzkarFreqInMins(int azkarFreqInMins){
        editor.putInt(Constants.AZKAR_FREQ_MINS,azkarFreqInMins).commit();
    }
    public int getAzkarFreqInMins(){return prefs.getInt(Constants.AZKAR_FREQ_MINS,1);}

    public String getFavoriteAzkar(){
        return prefs.getString(Constants.AZKAR,"");
    }
    public void setFavoriteAzkar(String azkar){
        editor.putString(Constants.AZKAR,azkar).commit();
    }

    public boolean isFavorite(WrittenZikr zikr){
        Type list = new TypeToken<List<WrittenZikr>>() {}.getType();
        List<WrittenZikr> azkar = gson.fromJson(getFavoriteAzkar(), list);
        if(azkar != null){
            for (WrittenZikr z : azkar){
                if (z.getTitle().equalsIgnoreCase(zikr.getTitle()))
                    return z.isFavorite();
            }
        }

        return zikr.isFavorite();
    }
}
