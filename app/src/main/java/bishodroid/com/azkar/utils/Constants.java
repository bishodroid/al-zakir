package bishodroid.com.azkar.utils;

/**
 * Created by hp on 26/11/2016.
 */

public class Constants {

    //App Settings
    public static final String APP_SETTINGS_NAME = "appSettings";
    public static final String FIRST_RUN_FLAG = "isFirstRun" ;
    public static final String AZKAR_SERVICE_FLAG = "isAzkarServiceRunning";
    public static final String APP_LANG = "appLanguage" ;
    public static final String AZKAR_FREQ_MINS = "azkarFrequencyInMinutes";

    //App languages
    public static final String LANG_AR = "ar";
    public static final String LANG_EN = "en";

    //Keys
    public static final String ZIKR_KEY = "writtenZikr";
    public static final String AZKAR = "azkar";
}
