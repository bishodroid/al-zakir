package bishodroid.com.azkar.utils;

import java.util.Comparator;

import bishodroid.com.azkar.pojos.Zikr;

/**
 * To compare azkar for sorting purpose
 */
public class ZikrComparator implements Comparator<Zikr> {

    @Override
    public int compare(Zikr o1, Zikr o2) {
        int z1 = Integer.parseInt(o1.getTitle().substring(4,5));
        int z2 = Integer.parseInt(o2.getTitle().substring(4,5));

        return z1 - z2;
    }
}