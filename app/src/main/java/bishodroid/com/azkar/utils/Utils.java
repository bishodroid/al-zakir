package bishodroid.com.azkar.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.util.DisplayMetrics;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bishodroid.com.azkar.R;
import bishodroid.com.azkar.pojos.WrittenZikr;
import bishodroid.com.azkar.pojos.Zikr;

/**
 * Created by hp on 26/11/2016.
 */

public class Utils {

    /**
     * Changes the apps language to the given langauge
     *
     * @param context
     * @param lang
     */
    public static void changeLanguage(Context context, String lang) {
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(lang.toLowerCase()));
        res.updateConfiguration(conf, dm);
    }

    /**
     * This will extract the azkar from the /res/raw directory and
     * create a list of {@link Zikr} objects.
     *
     * @param context
     * @return
     */
    public static List<Zikr> getAzkarList(Context context) {
        List<Zikr> azkar = new ArrayList<>();
        Field[] fields = R.raw.class.getFields();
        for (int i=1; i<fields.length; i++){
            Zikr zikr = null;
            try {
                if(fields[i].getName().startsWith("zikr")){
                    int resId = fields[i].getInt(fields[i]);
                    int duration = MediaPlayer.create(context,resId) != null ? MediaPlayer.create(context,resId).getDuration() : 0;
                    zikr = new Zikr(fields[i].getName(),resId,duration);
                    azkar.add(zikr);
                }

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return azkar;
    }

    /**
     * Gets the written azkar from the String array resource
     * @param context
     * @return List of {@link WrittenZikr}
     */
    public static List<WrittenZikr> getWrittenAzkar(Context context){
        List<WrittenZikr> writtenAzkar= new ArrayList<>();
        String[] titles = context.getResources().getStringArray(R.array.written_azkar_titles);
        String[] azkar = context.getResources().getStringArray(R.array.written_azkar_values);
        WrittenZikr zikr = null;
        for (int i=0; i<titles.length; i++){
            zikr = new WrittenZikr(false,azkar[i],titles[i]);
            writtenAzkar.add(zikr);
        }
        return writtenAzkar;
    }
}
