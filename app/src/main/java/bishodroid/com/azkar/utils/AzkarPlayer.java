package bishodroid.com.azkar.utils;

import android.content.Context;
import android.media.MediaPlayer;
import android.widget.Toast;

import java.util.List;

import bishodroid.com.azkar.R;
import bishodroid.com.azkar.pojos.Zikr;

/**
 * Created by user on 12/26/2016.
 */

public class AzkarPlayer {

    private static MediaPlayer player;
    private static AzkarPlayer singleton;
    private Context context;
    private static List<Zikr> azkarList;
    private static int currentZikrIndex = 0;
    private static int azkarSize = 0;


    private AzkarPlayer(Context context) {
        this.context = context;
    }

    /**
     * Creates a siingleton {@link AzkarPlayer} object
     *
     * @param context
     * @return
     */
    public static AzkarPlayer getInstance(Context context) {
        if (singleton == null) {
            singleton = new AzkarPlayer(context);
            azkarList = Utils.getAzkarList(context);
            azkarSize = azkarList.size();
            player = MediaPlayer.create(context, azkarList.get(currentZikrIndex).getFileId());
        }
        return singleton;
    }

    /**
     * Plays the previous zikr in the list
     */
    public void playPrevious() {
        if (currentZikrIndex <= 0)
            Toast.makeText(context, R.string.vocal_azkar_reached_first, Toast.LENGTH_LONG).show();
        else {
            playZikr(--currentZikrIndex);
        }

    }

    /**
     * Plays the next zikr in the list
     */
    public void playNext() {
        if (currentZikrIndex == azkarSize - 1) {
            Toast.makeText(context, R.string.vocal_azkar_reached_last, Toast.LENGTH_LONG).show();
            currentZikrIndex = 0;
            playZikr(currentZikrIndex);
        } else {
            playZikr(++currentZikrIndex);
        }
    }

    /**
     * Plays the zikr if it is paused, pauses it if it is playing
     */
    public void playPauseZikr() {
        if (player.isPlaying())
            player.pause();
        else
            player.start();

    }

    /**
     * Plays and pauses teh zikr
     */
    private void playZikr(int index) {
        player.stop();
        Zikr zikr = azkarList.get(index);
        player = MediaPlayer.create(context, zikr.getFileId());
        player.start();
    }

    public int getDuration() {
        return player.getDuration();
    }

    public int getCurrentPosition() {
        return player.getCurrentPosition();
    }

    public boolean isPlaying() {
        return player.isPlaying();
    }

    public String getTitle() {
        return azkarList.get(currentZikrIndex).getTitle();
    }
}
