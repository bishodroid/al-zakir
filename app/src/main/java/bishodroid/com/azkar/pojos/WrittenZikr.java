package bishodroid.com.azkar.pojos;

import java.io.Serializable;

/**
 * Created by user on 12/29/2016.
 */

public class WrittenZikr implements Serializable{

    private String title;
    private String zikr;
    private boolean isFavorite;

    public WrittenZikr(boolean isFavorite, String zikr, String title) {
        this.isFavorite = isFavorite;
        this.zikr = zikr;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getZikr() {
        return zikr;
    }
    public void setZikr(String zikr) {
        this.zikr = zikr;
    }
    public boolean isFavorite() {
        return isFavorite;
    }
    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public String toString() {
        return "WrittenZikr{" +
                "title='" + title + '\'' +
                ", zikr='" + zikr + '\'' +
                ", isFavorite=" + isFavorite +
                '}';
    }
}
