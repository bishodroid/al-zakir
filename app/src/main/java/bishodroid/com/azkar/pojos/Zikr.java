package bishodroid.com.azkar.pojos;

import java.util.Comparator;

/**
 * This is a bean class to hold the single zikr details to be used to populate the azkar list
 * Created by user on 12/25/2016.
 */
public class Zikr {

    private String title;
    private int duration;
    private int fileId;

    /**
     * Constructor to create a zikr file from the /res/raw dir
     * @param title
     * @param fileId
     * @param  duration
     */
    public Zikr(String  title, int fileId, int duration){
        this.title = title;
        this.fileId = fileId;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }
    public int getFileId() {
        return fileId;
    }
    public void setFileId(int fileId) {
        this.fileId = fileId;
    }
    @Override
    public String toString() {
        return "Zikr{" +
                "title='" + title + '\'' +
                ", duration=" + duration +
                ", fileId=" + fileId +
                '}';
    }
}
