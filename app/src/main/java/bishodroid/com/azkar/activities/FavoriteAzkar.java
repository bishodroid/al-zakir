package bishodroid.com.azkar.activities;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import bishodroid.com.azkar.R;
import bishodroid.com.azkar.pojos.WrittenZikr;
import bishodroid.com.azkar.utils.AppSettings;
import bishodroid.com.azkar.utils.Constants;
import bishodroid.com.azkar.utils.Utils;

public class FavoriteAzkar extends AppCompatActivity implements AdapterView.OnItemClickListener{


    private ListView azkarList;
    private Toolbar toolbar;
    private ActionBar actionbar;
    private String[] azkarTitles;
    private List<WrittenZikr> favs;
    private Gson gson;
    private AppSettings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_azkar);
        settings = AppSettings.getInstance(getApplicationContext());
        gson = new Gson();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            initGui21();
        } else {
            initGui();
        }
    }

    private void initGui21() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setTitle(R.string.app_name);
            actionbar.setIcon(R.mipmap.ic_launcher);
        }
        initGui();

    }

    private void initGui() {

        Type list = new TypeToken<List<WrittenZikr>>() {}.getType();
        favs= gson.fromJson(settings.getFavoriteAzkar(), list);
        azkarTitles = setAzkarTitles(favs);

        azkarList = (ListView) findViewById(R.id.favorite_azkar_list);
        azkarList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, azkarTitles));
        azkarList.setOnItemClickListener(this);
    }

    private String[] setAzkarTitles(List<WrittenZikr> favs){
        azkarTitles = new String[favs.size()];
        for (int i=0; i<favs.size(); i++){
            azkarTitles[i] = favs.get(i).getTitle();
        }
        return azkarTitles;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent i = new Intent(this, FullZikr.class);
        i.putExtra(Constants.ZIKR_KEY, favs.get(position));
        startActivity(i);
    }
}
