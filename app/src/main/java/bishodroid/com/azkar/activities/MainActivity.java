package bishodroid.com.azkar.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import bishodroid.com.azkar.R;
import bishodroid.com.azkar.utils.AppSettings;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Toolbar toolbar;
    private ActionBar bar;
    private AppSettings settings;
    private Button vocal,daily,favorites,add;
    private TextView intro,copyright;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        settings = AppSettings.getInstance(getApplicationContext());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            initGui21();
        }else {
            initGui();
        }
    }

    /**
     * Initializes the toolbar for devices > lollipop
     */
    private void initGui21() {
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        bar = getSupportActionBar();
        if (bar != null){
            setupToolbar();
        }
        initGui();
        updateWidgets();
    }

    /**
     * Initializes the GUI elements
     */
    private void initGui(){
        vocal = (Button)findViewById(R.id.main_vocal_azkar);
        vocal.setOnClickListener(this);
        daily = (Button)findViewById(R.id.main_daily_azkar);
        daily.setOnClickListener(this);
        favorites = (Button)findViewById(R.id.main_my_favorites);
        favorites.setOnClickListener(this);
        add = (Button)findViewById(R.id.main_add_azkar);
        add.setOnClickListener(this);
        intro = (TextView)findViewById(R.id.main_intro);
        copyright = (TextView)findViewById(R.id.main_copyright);
    }
    /**
     * Updates the widgets with the text of the selected language
     */
    private void updateWidgets(){
        vocal.setText(getString(R.string.main_vocal_azkar));
        daily.setText(getString(R.string.main_daily_azkar));
        favorites.setText(getString(R.string.main_my_azkar));
        add.setText(getString(R.string.main_add_azkar));
        intro.setText(R.string.main_intro);
        copyright.setText(getString(R.string.main_copy_rights));
        setupToolbar();
    }

    /**
     * Sets up the toolbar
     */
    private void setupToolbar(){
        if (bar ==  null) bar = getSupportActionBar();
        bar.setIcon(R.mipmap.ic_launcher);
        bar.setTitle(R.string.app_name);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateWidgets();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bar_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings){
            startActivity(new Intent(MainActivity.this,SettingsActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.main_vocal_azkar:
                startActivity(new Intent(MainActivity.this,VocalAzkar.class));
                break;
            case R.id.main_daily_azkar:
                startActivity(new Intent(MainActivity.this,DailyAzkar.class));
                break;
            case R.id.main_my_favorites:
                startActivity(new Intent(MainActivity.this,FavoriteAzkar.class));
                break;
        }
    }
}
