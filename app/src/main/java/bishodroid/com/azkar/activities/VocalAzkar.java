package bishodroid.com.azkar.activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import bishodroid.com.azkar.R;
import bishodroid.com.azkar.pojos.Zikr;
import bishodroid.com.azkar.services.AzkarService;
import bishodroid.com.azkar.utils.AppSettings;
import bishodroid.com.azkar.utils.AzkarPlayer;
import bishodroid.com.azkar.utils.Utils;
import bishodroid.com.azkar.utils.ZikrComparator;

public class VocalAzkar extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener,View.OnClickListener{

    private Toolbar toolbar;
    private ActionBar bar;
    private SwitchCompat switchCompat;
    private EditText timer;
    private ImageButton next,prev,play;
    private TextView zikrTitle,start,end;
    private SeekBar seekBar;

    private List<Zikr> azkarList;
    private static int currentZikrIndex = 0;
    private static int azkarSize = 0;
    private int startTime = 0;
    private int endTime = 0;

    private AzkarPlayer player;
    private Handler handler = new Handler();
    private AzkarService azkarService;
    private AppSettings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vocal_azkar);

        azkarList = Utils.getAzkarList(this);
        Collections.sort(azkarList,new ZikrComparator());
        azkarSize = azkarList.size();

        azkarService = AzkarService.getInstance(getApplicationContext());
        settings = AppSettings.getInstance(getApplicationContext());

        player = AzkarPlayer.getInstance(getApplicationContext());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            initGui21();
        }else {
            initGui();
        }
    }

    /**
     * Initialzes the GUI widgets for devices >= lollipop
     */
    private void initGui21(){
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        bar = getSupportActionBar();
        if (bar != null){
            bar.setIcon(R.mipmap.ic_launcher);
            bar.setTitle(R.string.app_name);
        }
        initGui();
    }

    /**
     * Initializes GUI widgets for devices < lollipop
     */
    private void initGui(){
        switchCompat = (SwitchCompat)findViewById(R.id.vocal_enable_vocal_azkar);
        switchCompat.setChecked(settings.isAzkarServiceRunning());
        switchCompat.setOnCheckedChangeListener(this);

        timer = (EditText)findViewById(R.id.vocal_freq_in_mins);
        timer.clearFocus();

        start = (TextView)findViewById(R.id.vocal_azkar_start);
        end = (TextView)findViewById(R.id.vocal_azkar_end);
        seekBar = (SeekBar)findViewById(R.id.vocal_azkar_seekbar);
        seekBar.setClickable(false);

        play = (ImageButton)findViewById(R.id.vocal_azkar_play);
        play.setImageDrawable(getResources().getDrawable(android.R.drawable.ic_media_play));
        play.setOnClickListener(this);

        next = (ImageButton)findViewById(R.id.vocal_azkar_next);
        next.setOnClickListener(this);

        prev = (ImageButton)findViewById(R.id.vocal_azkar_prev);
        prev.setOnClickListener(this);

        zikrTitle = (TextView)findViewById(R.id.zikr_title);
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.vocal_enable_vocal_azkar){
            if (isChecked){
                startService(new Intent(VocalAzkar.this,AzkarService.class));
                settings.setIsAzkarServiceRunning(isChecked);
            }else {
                stopService(new Intent(VocalAzkar.this,AzkarService.class));
                settings.setIsAzkarServiceRunning(isChecked);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.vocal_azkar_prev:
                player.playPrevious();
                updateWidgets(player.getTitle(),player.isPlaying(),player.getCurrentPosition(),player.getDuration());
                handler.postDelayed(UpdateSongTime,10);
                break;
            case R.id.vocal_azkar_next:
                player.playNext();
                updateWidgets(player.getTitle(),player.isPlaying(),player.getCurrentPosition(),player.getDuration());
                handler.postDelayed(UpdateSongTime,10);
                break;
            case R.id.vocal_azkar_play:
                player.playPauseZikr();
                updateWidgets(player.getTitle(),player.isPlaying(),player.getCurrentPosition(),player.getDuration());
                handler.postDelayed(UpdateSongTime,10);
                break;
        }
    }

    /**
     * Updates the mediaplay widgets to display the correct views
     * @param isPlaying
     * @param barMax
     */
    private void updateWidgets(String title, boolean isPlaying,int position,int barMax){
        int resId = isPlaying ? android.R.drawable.ic_media_pause : android.R.drawable.ic_media_play;
        play.setImageDrawable(getResources().getDrawable(resId));
        zikrTitle.setText(title);
        start.setText(minSec(position));
        end.setText(minSec(barMax));
        seekBar.setProgress(position);
        seekBar.setMax(barMax);
    }


    private String minSec(int duration){
        return String.format(String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes((long) duration),
                TimeUnit.MILLISECONDS.toSeconds((long) duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                duration))));
    }
    /**
     * Thread to update the media player widgets over time
      */
  private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            startTime = player.getCurrentPosition();
            start.setText(String.format(Locale.ENGLISH,"%d:%d",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                    toMinutes((long) startTime)))
            );
            seekBar.setProgress(startTime);
            handler.postDelayed(this, 10);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        initGui();
    }
}
