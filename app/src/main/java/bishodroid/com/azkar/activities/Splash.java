package bishodroid.com.azkar.activities;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.animation.ValueAnimatorCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import bishodroid.com.azkar.BuildConfig;
import bishodroid.com.azkar.R;
import bishodroid.com.azkar.utils.AppSettings;

/**
 * Splash activity
 */
public class Splash extends AppCompatActivity implements Animation.AnimationListener {

    private ImageView appLogo;
    private TextView appIntro, appVersion;
    private Animation pulse;

    private AppSettings settings;

    private static final int MAX_WAIT = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();

        Thread timer = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(MAX_WAIT);
                    if(settings.isFirstRun()){
                        Log.d("SPLASH","first run");
                        settings.setIsFirstRun(false);
                        startActivity(new Intent(Splash.this,ChooseLanguage.class));
                        finish();
                    }else{
                        Log.d("SPLASH","NOT first run");
                        startActivity(new Intent(Splash.this, MainActivity.class));
                        finish();
                    }
                } catch (InterruptedException e) {
                    Log.e(Splash.class.getName(),e.getMessage());
                }
            }
        });
        timer.start();
    }

    /**
     * Initializes the GUI components and necessary util classes
     */
    private void init() {
        settings = AppSettings.getInstance(getApplicationContext());

        appLogo = (ImageView) findViewById(R.id.splash_logo);
        appIntro = (TextView) findViewById(R.id.splash_title);
        appVersion = (TextView) findViewById(R.id.splash_version);

        String version = getResources().getString(R.string.version_prefix) +" "+ BuildConfig.VERSION_NAME;
        appVersion.setText(version);

        pulse = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.pulse);
        pulse.setAnimationListener(this);
        appLogo.startAnimation(pulse);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
