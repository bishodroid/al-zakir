package bishodroid.com.azkar.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bishodroid.com.azkar.R;
import bishodroid.com.azkar.pojos.WrittenZikr;
import bishodroid.com.azkar.pojos.Zikr;
import bishodroid.com.azkar.utils.AppSettings;
import bishodroid.com.azkar.utils.Constants;

public class FullZikr extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private TextView title, value;
    private CheckBox isFavorite;

    private AppSettings settings;
    private WrittenZikr zikr;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_zikr);

        zikr = (WrittenZikr) getIntent().getExtras().getSerializable((Constants.ZIKR_KEY));
        settings = AppSettings.getInstance(getApplicationContext());
        gson = new Gson();

        initGui();

    }


    private void initGui() {
        title = (TextView) findViewById(R.id.written_zikr_title);
        title.setText(zikr.getTitle());

        value = (TextView) findViewById(R.id.written_zikr_value);
        value.setText(zikr.getZikr());

        isFavorite = (CheckBox) findViewById(R.id.written_zikr_favorite);
        isFavorite.setChecked(settings.isFavorite(zikr));
        isFavorite.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String azkarJson = settings.getFavoriteAzkar();
        List<WrittenZikr> azkar;
        if (!azkarJson.trim().isEmpty()) {
            Type list = new TypeToken<List<WrittenZikr>>() {}.getType();
            azkar = gson.fromJson(azkarJson, list);
            for (int i = 0; i < azkar.size(); i++) {
                if (azkar.get(i).getTitle().equalsIgnoreCase(zikr.getTitle())) {
                    azkar.get(i).setFavorite(isChecked);
                    settings.setFavoriteAzkar(gson.toJson(azkar));
                    isFavorite.setChecked(isChecked);
                    Toast.makeText(this, isFavorite.isChecked() ? R.string.favorite_zikr_updated: R.string.zikr_added_to_favorites, Toast.LENGTH_SHORT).show();
                    break;
                }else if (i == azkar.size()-1 && !zikr.getTitle().equalsIgnoreCase(azkar.get(i).getTitle())){
                    zikr.setFavorite(isChecked);
                    azkar.add(zikr);
                    settings.setFavoriteAzkar(gson.toJson(azkar));
                    isFavorite.setChecked(isChecked);
                    Toast.makeText(this, R.string.zikr_added_to_favorites, Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        } else {
            azkar = new ArrayList<>();
            zikr.setFavorite(isChecked);
            azkar.add(zikr);
            String json = gson.toJson(azkar);
            settings.setFavoriteAzkar(json);
            isFavorite.setChecked(isChecked);
            Toast.makeText(this,R.string.first_time,Toast.LENGTH_SHORT).show();
        }
    }
}
