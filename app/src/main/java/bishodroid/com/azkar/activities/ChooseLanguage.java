package bishodroid.com.azkar.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import bishodroid.com.azkar.R;
import bishodroid.com.azkar.utils.AppSettings;
import bishodroid.com.azkar.utils.Constants;
import bishodroid.com.azkar.utils.Utils;

public class ChooseLanguage extends AppCompatActivity implements View.OnClickListener {

    private Button en, ar, next;
    private TextView text;
    private ImageView logo;

    private Animation pulse;
    private AppSettings settings;

    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);
        context = getApplicationContext();
        init();
        setUpClickListeners();
    }

    /**
     * Initializes the GUI components and necessary objects
     */
    private void init() {
        settings = AppSettings.getInstance(getApplicationContext());

        en = (Button) findViewById(R.id.choose_lang_en);
        ar = (Button) findViewById(R.id.choose_lang_ar);
        next = (Button) findViewById(R.id.choose_lang_next);
        text = (TextView) findViewById(R.id.choose_lang_text);
        logo = (ImageView) findViewById(R.id.choose_lang_logo);

        pulse = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.pulse);
        logo.startAnimation(pulse);
    }

    /**
     * Sets up the click listeners for the buttons
     */
    private void setUpClickListeners(){
        en.setOnClickListener(this);
        ar.setOnClickListener(this);
        next.setOnClickListener(this);
    }

    private void update(){
        en.setText(R.string.choose_lang_en);
        ar.setText(R.string.choose_lang_ar);
        text.setText(R.string.choose_lang_text);
        next.setText(R.string.choose_lang_next);
    }

    @Override
    public void onClick(View v) {
        String lang = "";
        switch (v.getId()) {
            case R.id.choose_lang_ar:
                Utils.changeLanguage(context, Constants.LANG_AR);
                lang = Constants.LANG_AR;
                update();
                break;
            case R.id.choose_lang_en:
                Utils.changeLanguage(context, Constants.LANG_EN);
                lang = Constants.LANG_EN;
                update();
                break;
            case R.id.choose_lang_next:
                settings.setAppLang(lang);
                startActivity(new Intent(ChooseLanguage.this,MainActivity.class));
                finish();
                break;
        }
    }

}
